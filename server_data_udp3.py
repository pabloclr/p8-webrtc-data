import asyncio
import json
from aiortc import RTCPeerConnection, RTCSessionDescription
import time

time_start = None

def current_stamp():
    global time_start
    if time_start is None:
        time_start = time.time()
        return 0
    else:
        return int((time.time() - time_start) * 1000000)

class SignallingProtocol(asyncio.DatagramProtocol):
    def __init__(self):
        self.pc = None
        self.transport = None
        self.data_channel = None
        self.setup_pc()

    def setup_pc(self):
        self.pc = RTCPeerConnection()
        self.pc.on("datachannel", self.on_datachannel)
        self.pc.on("iceconnectionstatechange", self.on_iceconnectionstatechange)

    def connection_made(self, transport):
        self.transport = transport
        print("Server registered and waiting for messages.")
        transport.sendto("REGISTER SERVER".encode(), ('127.0.0.1', 9999))

    def datagram_received(self, data, addr):
        message = data.decode()
        print(f"Received message: {message}")
        obj = json.loads(message)
        asyncio.ensure_future(self.handle_message(obj))

    async def handle_message(self, obj):
        if obj['type'] == 'offer':
            await self.pc.setRemoteDescription(RTCSessionDescription(**obj))
            await self.pc.setLocalDescription(await self.pc.createAnswer())
            response = json.dumps({"type": self.pc.localDescription.type, "sdp": self.pc.localDescription.sdp})
            self.transport.sendto(response.encode(), ('127.0.0.1', 9999))
        elif obj['type'] == 'answer':
            await self.pc.setRemoteDescription(RTCSessionDescription(**obj))
        elif obj['type'] == 'bye':
            print("Received BYE message from client. Resetting for new connections.")
            await self.pc.close()
            await self.reset_pc()

    def on_datachannel(self, channel):
        print(f"channel({channel.label}) > created by remote party")
        self.data_channel = channel
        channel.on("message", self.on_message)

    def on_message(self, message):
        print(f"channel({self.data_channel.label}) > {message}")
        if isinstance(message, str) and message.startswith("ping"):
            response = f"pong{message[4:]}"
            print(f"channel({self.data_channel.label}) > {response}")
            self.data_channel.send(response)

    def on_iceconnectionstatechange(self):
        if self.pc.iceConnectionState == 'failed':
            asyncio.ensure_future(self.pc.close())
            self.setup_pc()

    async def reset_pc(self):
        self.pc = RTCPeerConnection()
        self.setup_pc()

async def run_answer():
    loop = asyncio.get_running_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: SignallingProtocol(),
        local_addr=('127.0.0.1', 9997)
    )

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(run_answer())
        loop.run_forever()  # Keep the server running
    except KeyboardInterrupt:
        pass
    finally:
        loop.stop()
