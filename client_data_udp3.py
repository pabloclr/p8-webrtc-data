import asyncio
import json
import time
from aiortc import RTCPeerConnection, RTCSessionDescription

time_start = None

def current_stamp():
    global time_start
    if time_start is None:
        time_start = time.time()
        return 0
    else:
        return int((time.time() - time_start) * 1000000)

class SignallingProtocol(asyncio.DatagramProtocol):
    def __init__(self, pc, channel):
        self.pc = pc
        self.channel = channel
        self.message_count = 0

    def connection_made(self, transport):
        self.transport = transport
        print("Client registered and waiting for messages.")
        transport.sendto("REGISTER CLIENT".encode(), ('127.0.0.1', 9999))
        asyncio.ensure_future(self.send_offer())

    def datagram_received(self, data, addr):
        message = data.decode()
        print(f"Received message: {message}")

        obj = json.loads(message)
        asyncio.ensure_future(self.handle_message(obj))

    async def handle_message(self, obj):
        if obj['type'] == 'offer':
            await self.pc.setRemoteDescription(RTCSessionDescription(**obj))
            await self.pc.setLocalDescription(await self.pc.createAnswer())
            response = json.dumps({"type": self.pc.localDescription.type, "sdp": self.pc.localDescription.sdp})
            self.transport.sendto(response.encode(), ('127.0.0.1', 9999))
        elif obj['type'] == 'answer':
            await self.pc.setRemoteDescription(RTCSessionDescription(**obj))

    async def send_offer(self):
        await self.pc.setLocalDescription(await self.pc.createOffer())
        offer = json.dumps({"type": self.pc.localDescription.type, "sdp": self.pc.localDescription.sdp})
        self.transport.sendto(offer.encode(), ('127.0.0.1', 9999))

    def check_message_count(self):
        self.message_count += 1
        if self.message_count >= 3:
            bye_message = json.dumps({"type": "bye"})
            self.transport.sendto(bye_message.encode(), ('127.0.0.1', 9999))
            print("Sent BYE message")
            self.transport.close()
            self.channel.close()
            asyncio.get_running_loop().stop()

async def run_offer(pc):
    loop = asyncio.get_running_loop()

    channel = pc.createDataChannel("chat")
    print(f"channel({channel.label}) > created by local party")

    transport, protocol = await loop.create_datagram_endpoint(
        lambda: SignallingProtocol(pc, channel),
        local_addr=('127.0.0.1', 9998)
    )

    async def send_pings():
        while True:
            message = f"ping {current_stamp()}"
            print(f"channel({channel.label}) > {message}")
            channel.send(message)
            await asyncio.sleep(1)

    @channel.on("open")
    def on_open():
        asyncio.ensure_future(send_pings())

    @channel.on("message")
    def on_message(message):
        print(f"channel({channel.label}) > {message}")
        protocol.check_message_count()
        if isinstance(message, str) and message.startswith("pong"):
            elapsed_ms = (current_stamp() - int(message[5:])) / 1000
            print(" RTT %.2f ms" % elapsed_ms)

if __name__ == "__main__":
    pc = RTCPeerConnection()
    coro = run_offer(pc)

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
        loop.run_forever()  # Keep the client running
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
