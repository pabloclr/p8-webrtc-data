import asyncio
import json
from aiortc import RTCPeerConnection, RTCSessionDescription

time_start = None

def current_stamp():
    global time_start
    if time_start is None:
        time_start = time.time()
        return 0
    else:
        return int((time.time() - time_start) * 1000000)

class SignallingProtocol(asyncio.DatagramProtocol):
    def __init__(self, pc):
        self.pc = pc

    def connection_made(self, transport):
        self.transport = transport
        print("Server registered and waiting for messages.")
        transport.sendto("REGISTER SERVER".encode(), ('127.0.0.1', 9999))

    def datagram_received(self, data, addr):
        message = data.decode()
        print(f"Received message: {message}")

        obj = json.loads(message)
        asyncio.ensure_future(self.handle_message(obj))

    async def handle_message(self, obj):
        if obj['type'] == 'offer':
            await self.pc.setRemoteDescription(RTCSessionDescription(**obj))
            await self.pc.setLocalDescription(await self.pc.createAnswer())
            response = json.dumps({"type": self.pc.localDescription.type, "sdp": self.pc.localDescription.sdp})
            self.transport.sendto(response.encode(), ('127.0.0.1', 9999))
        elif obj['type'] == 'answer':
            await self.pc.setRemoteDescription(RTCSessionDescription(**obj))

async def run_answer(pc):
    loop = asyncio.get_running_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: SignallingProtocol(pc),
        local_addr=('127.0.0.1', 9997)
    )

    @pc.on("datachannel")
    def on_datachannel(channel):
        print(f"channel({channel.label}) > created by remote party")

        @channel.on("message")
        def on_message(message):
            print(f"channel({channel.label}) > {message}")

            if isinstance(message, str) and message.startswith("ping"):
                message = f"pong{message[4:]}"
                print(f"channel({channel.label}) > {message}")
                channel.send(message)

if __name__ == "__main__":
    pc = RTCPeerConnection()
    coro = run_answer(pc)

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
        loop.run_forever()  # Keep the server running
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
