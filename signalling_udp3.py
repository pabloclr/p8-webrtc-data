import asyncio
import json

clients = {}
servers = {}

class SignallingProtocol(asyncio.DatagramProtocol):
    def connection_made(self, transport):
        self.transport = transport
        print("Signalling server started on 127.0.0.1:9999")

    def datagram_received(self, data, addr):
        global clients, servers
        message = data.decode()
        print(f"Received {message} from {addr}")

        if message == "REGISTER CLIENT":
            clients["addr"] = addr
            print(clients)
            print(f"Registered client {addr}")
        elif message == "REGISTER SERVER":
            servers["addr"] = addr
            print(servers)
            print(f"Registered server {addr}")
        else:
            try:
                msg_json = json.loads(message)
                if msg_json["type"] == "offer" and servers:
                    self.transport.sendto(data, servers["addr"])
                elif msg_json["type"] == "answer" and clients:
                    self.transport.sendto(data, clients["addr"])
                elif msg_json["type"] == "bye" and servers:
                    self.transport.sendto(data, servers["addr"])
                    clients.pop("addr")
                    print(f"Removed client {addr}")
                    print(clients)
                    for server in servers:
                        self.transport.sendto(data, servers["addr"])
            except json.JSONDecodeError as e:
                print(f"Failed to decode JSON: {e}")

async def main():
    loop = asyncio.get_running_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: SignallingProtocol(),
        local_addr=('127.0.0.1', 9999)
    )
    try:
        await asyncio.sleep(3600)  # Mantiene el servidor funcionando por 1 hora
    finally:
        transport.close()

if __name__ == "__main__":
    asyncio.run(main())
