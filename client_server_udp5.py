import asyncio
import json
import sys
from aiortc import RTCPeerConnection, RTCSessionDescription

class SignallingProtocol(asyncio.DatagramProtocol):
    def __init__(self, pc, server_name):
        self.pc = pc
        self.server_name = server_name
        self.data_channel = None

    def connection_made(self, transport):
        self.transport = transport
        print("Server registered and waiting for messages.")
        register_message = json.dumps({"type": "register server", "server_name": self.server_name})
        transport.sendto(register_message.encode(), ('127.0.0.1', 9999))

    def datagram_received(self, data, addr):
        message = data.decode()
        print(f"Received message: {message}")

        obj = json.loads(message)
        asyncio.ensure_future(self.handle_message(obj, addr))

    async def handle_message(self, obj, addr):
        if obj['type'] == 'offer':
            server_name = obj['server_name']
            offer = {k: obj[k] for k in obj if k != "server_name"}  # Remove server_name
            await self.pc.setRemoteDescription(RTCSessionDescription(**offer))
            await self.pc.setLocalDescription(await self.pc.createAnswer())
            response = json.dumps({"type": self.pc.localDescription.type, "sdp": self.pc.localDescription.sdp, "server_name": server_name})
            print(response)
            self.transport.sendto(response.encode(), addr)
            self.setup_pc_callbacks()
        elif obj['type'] == 'answer':
            answer = {k: obj[k] for k in obj if k != "server_name"}  # Remove server_name
            await self.pc.setRemoteDescription(RTCSessionDescription(**answer))

    def setup_pc_callbacks(self):
        @self.pc.on("datachannel")
        def on_datachannel(channel):
            print(f"channel({channel.label}) > created by remote party")
            self.data_channel = channel

            @channel.on("message")
            def on_message(message):
                print(f"channel({channel.label}) > {message}")

                if isinstance(message, str) and message.startswith("ping"):
                    message = f"pong{message[4:]}"
                    print(f"channel({channel.label}) > {message}")
                    channel.send(message)

async def run_server(server_name):
    pc = RTCPeerConnection()
    loop = asyncio.get_running_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: SignallingProtocol(pc, server_name),
        local_addr=('127.0.0.1', 0)
    )
    protocol.setup_pc_callbacks()

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: client_server_udp5.py <server_name>")
        sys.exit(1)

    server_name = sys.argv[1]
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(run_server(server_name))
        loop.run_forever()  # Keep the server running
    except KeyboardInterrupt:
        pass
    finally:
        loop.stop()
