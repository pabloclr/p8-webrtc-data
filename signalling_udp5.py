import asyncio
import json

clients = {}
servers = {}

class SignallingProtocol(asyncio.DatagramProtocol):
    def connection_made(self, transport):
        self.transport = transport
        print("Signalling server started on 127.0.0.1:9999")

    def datagram_received(self, data, addr):
        message = data.decode()
        print(f"Received {message} from {addr}")

        try:
            msg_json = json.loads(message)
            if msg_json["type"] == "register server" and "server_name" in msg_json:
                servers[msg_json["server_name"]] = addr
                print(f"Servers:{servers}")
                print(f"Registered server {msg_json['server_name']} at {addr}")
                # Check if there are pending requests for this server


            elif msg_json["type"] == "register client" and "server_name" in msg_json:
                clients[msg_json["server_name"]] = addr
                print(f"Clientes:{clients}")
                print(f"Registered client for {msg_json['server_name']} at {addr}")
                # Check if there are pending requests for this server



            elif msg_json["type"] == "offer" and "server_name" in msg_json:
                server_name = msg_json["server_name"]
                if server_name in servers:
                    self.transport.sendto(data, servers[server_name])
                else:
                    if server_name not in clients:
                        clients[server_name] = []
                    clients[server_name].append({"sdp": data, "addr": addr})
                    print(f"Stored offer for server {server_name} from {addr}")
            elif msg_json["type"] == "answer" and "server_name" in msg_json:
                server_name = msg_json["server_name"]
                if server_name in clients:
                    self.transport.sendto(data, clients[server_name])

        except json.JSONDecodeError as e:
            print(f"Failed to decode JSON: {e}")

async def main():
    loop = asyncio.get_running_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: SignallingProtocol(),
        local_addr=('127.0.0.1', 9999)
    )
    try:
        await asyncio.sleep(3600)  # Keep the server running for 1 hour
    finally:
        transport.close()

if __name__ == "__main__":
    asyncio.run(main())
